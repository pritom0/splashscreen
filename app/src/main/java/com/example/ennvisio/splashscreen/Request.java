package com.example.ennvisio.splashscreen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONObject;
import org.json.JSONException;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;





import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Request extends AppCompatActivity {

    protected Button submit_button;
    String myString = "We will Contact With You Soon";
    private  String name,phone,address,category,service;

    private EditText etname,etphone;



  //RequestBody requestBody;

    RequestQueue queue;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);



        queue = Volley.newRequestQueue(this);

        submit_button =(Button) findViewById(R.id.submit);

        etname =(EditText) findViewById( R.id.etname );
        etphone = (EditText) findViewById( R.id.etphone );





        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.service_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);



        Spinner spinner_category = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.category_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_category.setAdapter(adapter2);

        final Spinner spinner_address = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.address_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_address.setAdapter(adapter3);







        spinner_address.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

/*
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),myString,Toast.LENGTH_LONG).show();

            }
        });


*/

        submit_button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name=etname.getText().toString();
                phone=etphone.getText().toString();


                address=spinner_address.getSelectedItem().toString();
                Log.d( "Pritom","Value "+name+" "+phone+address );

                RequestQueue requestQueue = Volley.newRequestQueue( Request.this );

               // DataInsert(name, phone);

            }
        } );






    }

    public void insertData() {
        String SERVER_URL = "http://localhost/api.php";
        StringRequest sq = new StringRequest( com.android.volley.Request.Method.POST, SERVER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        } ) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put( "name", name );
                params.put( "phone", phone );

                System.out.print( params );
                return params;
            }
        };
    }


/*
                public byte[]  getBody() throws AuthFailureError
                {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                requestQueue.add( sq );
    }

    /*
    public void DataInsert(final String name , final String phone)
    {
        String url = "http://localhost/api.php";
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("UResponse", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", response);

                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("token", sharedpreferences.getString(TokenPreference, ""));
                headers.put( "name",name );
                headers.put( "phone",phone );

                return headers;
            }



        };
        queue.add(postRequest);
    }
    */



}
