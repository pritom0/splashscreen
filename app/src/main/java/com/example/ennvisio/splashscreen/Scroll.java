package com.example.ennvisio.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.lang.*;

public class Scroll extends AppCompatActivity {

    Button Service,Process,Client,CallCenter,Booking ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll);


        Service = (Button)findViewById(R.id.button2);
        Service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Scroll.this,Service.class);
                startActivity(i);
            }
        });

        Process = (Button)findViewById(R.id.button3);
        Process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Scroll.this,Procedur.class);
                startActivity(i);
            }
        });


        Client = (Button)findViewById(R.id.button4);
        Client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Scroll.this,Client.class);
                startActivity(i);
            }
        });


        CallCenter = (Button)findViewById(R.id.button5);
        CallCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Scroll.this,CallCenter.class);
                startActivity(i);
            }
        });

        Booking = (Button)findViewById(R.id.booking);
        Booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Scroll.this,Booking.class);
                startActivity(i);
            }
        });


    }
}
