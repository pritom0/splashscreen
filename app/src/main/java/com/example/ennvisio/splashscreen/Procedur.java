package com.example.ennvisio.splashscreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.content.Intent;
import android.widget.AdapterView;

public class Procedur extends AppCompatActivity {

    int images[]={R.drawable.page01,R.drawable.page02,R.drawable.page03,R.drawable.page04,
            R.drawable.page05,R.drawable.page06};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_process );

        ListView listView = (ListView)findViewById( R.id.listView );

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter( customAdapter );


    }

    public class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup parent) {
            view= getLayoutInflater().inflate( R.layout.customlayout2,null );
            ImageView imageView=(ImageView)view.findViewById( R.id.imageView );

            imageView.setImageResource( images[i] );


            return view;
        }
    }
}
