package com.example.ennvisio.splashscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Service extends AppCompatActivity {


    int images[]= {R.drawable.electrical, R.drawable.plumbing,R.drawable.painting,
            R.drawable.carpentry,R.drawable.cleaning,R.drawable.renovation};
   // String names[]={"ELECTRICAL","PLUMBING","PAINTING","CARPENTARY","CLEANING","RENOVATION"};

  //  String code [] = {"SERVICE 1","SERVICE 2","SERVICE 3","SERVICE 4","SERVICE 5","SERVICE 6"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        ListView listView =(ListView)findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);


    }

    class CustomAdapter extends BaseAdapter{



        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout,null);

            ImageView imageView =(ImageView)view.findViewById(R.id.imageView);

            imageView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent( Service.this,Booking.class );
                    startActivity( i );
                }
            } );
         //   TextView textView_names  = (TextView)view.findViewById(R.id.textView_names);
        //   TextView textView_codes  = (TextView)view.findViewById(R.id.textView_codes);

            imageView.setImageResource(images[i]);
           // textView_names.setText(names[i]);
          //  textView_codes.setText(code[i]);








            return view;
        }
    }


}
