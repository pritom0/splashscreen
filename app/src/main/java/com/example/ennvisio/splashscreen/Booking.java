package com.example.ennvisio.splashscreen;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.HttpResponse;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;
import com.android.volley.toolbox.JsonArrayRequest;
import com.loopj.android.http.RequestParams;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class Booking extends AppCompatActivity {

    protected Button submit_button;
    String myString = "We will Contact With You Soon";
    private  String name,phone,address,category,service,road,block,home;

    private EditText mName,mPhone,mHome;
    Spinner spinner_service , spinner_category, spinner_address,spinner_road,spinner_block;


    JsonRequest requestBody;

    RequestQueue queue;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);





        queue = Volley.newRequestQueue(this);

        submit_button =(Button) findViewById(R.id.submit);

        mName =(EditText) findViewById( R.id.etname );
        mPhone = (EditText) findViewById( R.id.etphone );
        mHome = (EditText) findViewById( R.id.ethome );





        spinner_service = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.service_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_service.setAdapter(adapter);



        spinner_category = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.category_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_category.setAdapter(adapter2);

        spinner_address = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.address_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_address.setAdapter(adapter3);

        spinner_road = (Spinner) findViewById(R.id.spinner4);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this,
                R.array.road_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_road.setAdapter(adapter4);

        spinner_block = (Spinner) findViewById(R.id.spinner_block);
        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(this,
                R.array.block_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_block.setAdapter(adapter5);








/*
        spinner_address.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_service.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/







        submit_button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name=mName.getText().toString();
                phone=mPhone.getText().toString();
                home = mHome.getText().toString();

                //.... submit to reset all spinner ...............
                //reset();
                //
                    Toast.makeText( getApplicationContext(),myString,Toast.LENGTH_LONG). show();

                address=spinner_address.getSelectedItem().toString();
                service=spinner_service.getSelectedItem().toString();
                category=spinner_category.getSelectedItem().toString();
                road    = spinner_road.getSelectedItem().toString();
                block   = spinner_block.getSelectedItem().toString();
                Log.d( "Pritom","Value: "+name+" "+phone+" "+address+" "+service+" "+category+" "+road+" "+block+" "+home );

                // RequestQueue requestQueue = Volley.newRequestQueue( Request.this );
                UserModel umodel = new UserModel();

                umodel.name = name;
                umodel.phone = phone;
                umodel.address=address;
                umodel.service=service;
                umodel.category=category;
                umodel.road = road;
                umodel.block = block;
                umodel.home = home;

                DataInsert(umodel);

            }
        } );







    }
    // alertdialogue for exit.
    @Override
    public void onBackPressed()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder( Booking.this );
        builder.setMessage( "Are You Want To Exit Booking ?" );
        builder.setCancelable( true );
        builder.setNegativeButton( "Yes", new DialogInterface.OnClickListener() {
           // public DialogInterface dialogInterface;

            @Override
            public void onClick(DialogInterface dialog, int i) {

              finish();
            }
        } );
       /* builder.setPositiveButton( "Close!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                finish();
            }
        } );*/
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

   /* public void reset()
    {
        spinner_service.setSelection(0);
        spinner_category.setSelection(0);
        spinner_address.setSelection(0);
        spinner_road.setSelection(0);
        spinner_block.setSelection(0);
    }*/

    /*public void insertData()
    {
         String SERVER_URL = "http://localhost/api.php";
                StringRequest sq = new StringRequest( com.android.volley.Request.Method.POST, SERVER_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                } ) {
                    protected Map<String,String> getParams() throws AuthFailureError
                    {Map<String,String> params = new HashMap<>(  );
                       params.put( "name",name );
                       params.put( "phone",phone );

                       System.out.print( params );
                        return  params;
                    }
                };


                public byte[]  getBody() throws AuthFailureError
                {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee)
                    {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                requestQueue.add( sq );
    }
*/
    public RequestParams DataInsert(final UserModel model)
    {
        String url = "http://amarmistriapi.ennvisiodigital.tech/amrmistri.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("UResponse", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        //  Log.d("Error.Response",response);

                    }
                }
        )


                //pritom edit
        {
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put( "name",model.name );
                params.put( "phone",model.phone );
                params.put( "address",model.address );
                params.put( "service",model.service );
                params.put( "category",model.category );
                params.put( "road",model.road );
                params.put( "block",model.block );
                params.put( "home",model.home );

                return params;
            }


                 // close app

        };










/*
        {
           @Override
           public Map<String, String> getHeaders() throws AuthFailureError {
              HashMap<String, String> headers = new HashMap<String, String>();

                //headers.put("token", sharedpreferences.getString(TokenPreference, ""));
             Log.d( "VolleyServer","Value header name: "+model.name+" phone "+model.phone);

              headers.put( "name",model.name );
               headers.put( "phone",model.phone );


               return headers;
            }

             @Override
              public byte[] getBody() throws AuthFailureError {


               Gson gson = new Gson();
                   Log.d( "VolleyServer","Value body name: "+model.name+" phone "+model.phone);
                   //gson.toJson(user);
                   String mContent = gson.toJson(model);
                   Log.d("mcontent","content:"+mContent);
                   byte[] body = new byte[0];
                   try {
                       body = mContent.getBytes("UTF-8");
                  } catch (UnsupportedEncodingException e) {
                       Log.e("pp", "Unable to gets bytes from JSON", e.fillInStackTrace());
                   }
                   return body;
               }




        };*/
        queue.add(postRequest);
        return null;
    }



}
