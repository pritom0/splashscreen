package com.example.ennvisio.splashscreen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CallCenter extends AppCompatActivity {
    private static final int REQUEST_CALL =1 ;
    Intent CallIntent;

    Button mCallButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_call_center );
        init();
    }

    private void init(){

        mCallButton = (Button)findViewById( R.id.call );
        mCallButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallIntent = new Intent( Intent.ACTION_CALL, Uri.parse( "tel:+88 02 9615252 " ) );
                if (ContextCompat.checkSelfPermission( CallCenter.this, Manifest.permission.CALL_PHONE )!= PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions( CallCenter.this,new String[]{Manifest.permission.CALL_PHONE},REQUEST_CALL);
                } else {
                    startActivity( CallIntent );
                }
            }
        } );

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CALL:
            {
                if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    startActivity( CallIntent );

                }
                else{

                }
            }
        }
    }
}
